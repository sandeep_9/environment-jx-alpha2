var env = {
  DOMAIN_TITLE: 'LivSpace Authentication',
  DOMAIN_URL: 'https://livspace-web.alpha.livspace.com/my-account',
  API_URL: 'https://accounts-backend.alpha.livspace.com',
  SENTRY_ENV: 'alpha',
  SENTRY_URL: 'https://47f8bf171a524499a57c25d9f6563a37@sentry.livspace.com/33'
}